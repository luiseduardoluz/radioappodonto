app.filter('trusted', ['$sce', function ($sce) {
  return function (url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);

angular.module('starter.controllers', [])

  .controller('AppCtrl', function ($scope, $ionicModal, $timeout) {

  })

  .controller('HomeCtrl', function ($scope, $ionicModal, $timeout, $state, REST) {


  })


  .controller('PaginaCtrl', function ($scope, $stateParams, $timeout, $state, REST) {
    //console.log($stateParams.pagina);
    $scope.pagina = $stateParams.pagina;
    $scope.next = function () {
      $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function () {
      $ionicSlideBoxDelegate.previous();
    };
    $scope.slideChanged = function (index) {
      $scope.slideIndex = index;
    };

    $scope.goPag = function (url) {
      $timeout(function () {
        $state.go('app.pagina', {
          pagina: url
        });
      });
    };

    REST.getJson('paginas.json').then(
      function success(data) {
        if ($scope.pagina != '') {
          for (var i = 0; i < data.length; i++) {
            if (data[i].slug === $scope.pagina) {
              $scope.data = data[i];
              $scope.titulo = $scope.data.title;
            }
          }
        } else {
          $scope.data = data[0]; // home
        }
      },
      function error(errResponse) {
        console.error('Error while fetching Currencies');
      }
    );
  });
