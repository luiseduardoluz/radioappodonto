(function () {


  var module = angular.module('REST', [])

  module.factory('REST', ['$http', '$q', '$location', function ($http, $q, $location) {

    var baseUrl = '';
    if (ionic.Platform.isAndroid()) {
      //baseUrl = "/android_asset/www/";
    }

    return {

      getJson: function (url) {
        var ret = $q.defer();

        $http({
          method: 'GET',
          url: baseUrl + url,
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          }
        }).then(function successCallback(response) {
          ret.resolve(response.data);

        }, function errorCallback(response) {
          console.log(response)
          ret.reject(response);
        });

        return ret.promise;
      }
    }
  }])
})();
